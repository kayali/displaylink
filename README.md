# displaylink-debian

DisplayLink driver installer for Debian and Ubuntu based Linux distributions: Debian, Ubuntu, Elementary OS, Mint, Kali, Deepin and more!

## Installation
1. Download the repo, cd in the directory and run the shell script with sudo:
```shell
git clone https://gitlab.gwdg.de/kayali/displaylink.git
cd displaylink
sudo chmod +x displaylink-debian.sh
sudo ./displaylink-debian.sh --install
```
2. type _y_ when prompted to install the dependencies and to agree to DisplayLink [licensing terms](https://www.synaptics.com/products/displaylink-graphics/downloads/ubuntu-5.7?filetype=exe)
## Uninstallation
```shell
sudo ./displaylink-debian.sh --uninstall or sudo sh displaylink-debian.sh --uninstall
```

## Supported platforms are:

  * Debian: Jessie 8.0/Stretch 9.0/Buster 10/Bullseye 11/Bookworm (testing)/Sid (unstable)
  * Ubuntu: 14.04 Trusty - 23.04 Lunar
  * elementary OS: 0.3 Freya- 7.0 Horus
  * Mint: 15 Olivia / 21.1 Vera
  * LMDE: 2 Betsy - 5 Elsie
  * Kali: kali-rolling/2016.2 - 2023.1
  * Deepin: stable - unstable
  * UOS: apricot - eagle
  * MX Linux: 17.1/18
  * BunsenLabs: Helium - Beryllium
  * Parrot: 4.5 - 5+
  * Devuan: ASCII - Chimaera
  * Pop!_OS: 20.04 Focal - 22.04 Jammy
  * PureOS: 9 Amber - 10 Byzantium
  * Nitrux: nitrux
  * Zorin: focal
